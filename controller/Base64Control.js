var buffer = require('buffer');
var path = require('path');
var fs = require('fs');

exports.decodeBase64 = (base64str, filename) => {

    var buf = Buffer.from(base64str,'base64');

    fs.writeFile(path.join(__dirname,'../public/images',filename), buf, function(error){
        if(error){
            throw error;
        }else{
            console.log('File created from base64 string!');
            return true;
        }
    });
}
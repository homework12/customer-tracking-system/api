var express = require('express');
var router = express.Router();

const _ = require('lodash');
const base64Control=require('../controller/Base64Control')

var nowDate = require('../utils/Date')
var makeDifferentID = require('../utils/MakeID')

router.get('/list/:categoryID',GetProductList)
router.get('/:productID',GetProduct)
router.post('/',AddProduct)
router.put('/',UpdateProduct)
router.delete('/:productID',DeleteProduct)

function GetProductList(req, res, next){
    var sql="Select productID,productName,productCount from product where categoryID=?"
    db.query(sql,req.params.categoryID,function (err,result) {
        if (err){
            console.log(err)
            res.json({response :- 2 })
        }
        else if(result==''){
            res.json({response :0})
        }
        else {
            res.json({response :1,list:result})
        }
    })
}

function GetProduct(req,res,next) {

    var sql="Select p.productName, p.productCount, p.productExplanation, sc.categoryName, p.productImage, p.buyingPrice, p.sellingPrice,DATE_FORMAT(p.creationDate ,'%d/%m/%Y') as creationDate  from product p Inner JOIN stock_category sc on p.categoryID = sc.categoryID where p.productID=?"
    db.query(sql,req.params.productID,function (err,result) {
        if (err){
            console.log(err)
            res.json({response: -2})
        }
        else if(result==''){
            res.json({response:0})
        }
        else {
            res.json({response:1,product:result[0]})
        }
    })

}

function AddProduct(req, res, next){
    var creationDate= nowDate.getNowDate()
    var productImage=req.body.productImage
    if(productImage!='-1'){
        var image_name=makeDifferentID.makeId()+nowDate.getNowForPath()+".jpg"
        var image_path ="productImage/"+image_name
        base64Control.decodeBase64(req.body.productImage,image_path)
        var data=[req.body.productName,req.body.productCount,req.body.productExplanation, req.body.categoryID,image_path ,req.body.buyingPrice,req.body.sellingPrice,creationDate]
        var sql="Insert into product(productName, productCount, productExplanation, categoryID, productImage, buyingPrice, sellingPrice, creationDate) VALUES (?,?,?,?,?,?,?,?)"
        db.query(sql,data,function (err,result) {
            if (err){
                console.log(err)
                res.json({response:-2})
            }
            else {
                res.json({response:1})
            }
        })
    }
    else {
        var data=[req.body.productName,req.body.productCount,req.body.productExplanation, req.body.categoryID,-1 ,req.body.buyingPrice,req.body.sellingPrice,creationDate]
        var sql="Insert into product(productName, productCount, productExplanation, categoryID, productImage, buyingPrice, sellingPrice, creationDate) VALUES (?,?,?,?,?,?,?,?)"
        db.query(sql,data,function (err,result) {
            if (err){
                console.log(err)
                res.json({response:-2})
            }
            else {
                res.json({response:1})
            }
        })
    }

}

function UpdateProduct(req,res,next) {
    var productID=req.body.productID
    var productImage = req.body.productImage
    var updatedProduct={}
    if(productImage!='-1'){
        var image_name=makeDifferentID.makeId()+nowDate.getNowForPath()+".jpg"
        var image_path ="productImage/"+image_name
        base64Control.decodeBase64(productImage,image_path)
        _(req.body).forEach((value, key) => {
            if (!_.isEmpty(value)) {
                console.log(key)
                if(key=='productImage'){
                    console.log(1)
                    updatedProduct[key]=image_path
                }
            else
                updatedProduct[key] = value;
            }
        });
        var sql = "Update product Set ? where productID=" + productID
        db.query(sql,[updatedProduct],function (err,result) {
            if(err){
                console.log(err)
                res.json({response:-2})
            }
            else {
                res.json({response: 1})
            }
        })
    }
    else {
        _(req.body).forEach((value, key) => {
            if (!_.isEmpty(value)) {
                updatedProduct[key] = value;
            }
        });
        var sql = "Update product Set ? where productID=" + productID
        db.query(sql,[updatedProduct],function (err,result) {
            if(err){
                console.log(err)
                res.json({response:-2})
            }
            else {
                res.json({response: 1})
            }
        })
    }

}

function DeleteProduct(req,res,next) {
    var sql="Delete from product where productID=?"
    db.query(sql,req.params.productID,function (err,result) {
        if (err){
            console.log(err)
            res.json({response: -2})
        }
        else {
            res.json({response:1})
        }
    })
}


module.exports=router
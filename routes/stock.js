var express = require('express');
var router = express.Router();

var nowDate = require('../utils/Date')

router.get('/:userID',GetStockCategory)
router.post('/',AddStockCategory)
router.put('/',UpdateStockCategory)
router.delete('/:categoryID',DeleteStockCategory)

function GetStockCategory(req, res, next){
    var sql="Select sc.categoryID,sc.categoryName,(Select COUNT(*) from product p where p.categoryID=sc.categoryID) as stockProductCount, DATE_FORMAT(sc.creationDate ,'%d/%m/%Y') as creationDate  from stock_category sc where sc.userID=?"
    db.query(sql,req.params.userID,function (err,result) {
        if (err){
            console.log(err)
            res.json({response :- 2 })
        }
        else if(result==''){
            res.json({response :0})
        }
        else {
            res.json({response :1,list:result})
        }
    })
}

function AddStockCategory(req, res, next){
    var creaionDate = nowDate.getNowDate()
    var sql="Insert into stock_category(userID, categoryName, creationDate) values (?,?,?)"
    db.query(sql,[req.body.userID,req.body.categoryName,creaionDate],function (err,result) {
        if (err){
            console.log(err)
            res.json({response: -2})
        }
        else {
            res.json({response: 1})
        }
    })
}

function UpdateStockCategory(req, res, next){
    console.log(req.body)
    var sql = "Update stock_category set categoryName =? where categoryID=?"
    db.query(sql,[req.body.categoryName,req.body.categoryID],function (err,result) {
        if (err){
            console.log(err)
            res.json({response: -2})
        }
        else {
            res.json({response:1})
        }
    })
}

function DeleteStockCategory(req, res, next) {
    var categoryID=req.params.categoryID
    var deleteCategory_sql="Delete from stock_category where categoryID=?"
    db.query(deleteCategory_sql,categoryID ,function (err,result) {
        if (err){
            console.log(err)
            res.json({response : -2})
        }
        else{
            var deleteProductsql = "Delete from product where categoryID=?"
            db.query(deleteProductsql,categoryID,function (err,result) {
                if (err){
                    console.log(err)
                    res.json({response : -2})
                }
                else {
                    res.json({response : 1})
                }
            })
        }
    })
}

module.exports=router
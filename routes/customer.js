var express = require('express');
var router = express.Router();

const date = require('../utils/Date')

router.post('/',AddCustomer)
router.post('/info', GetCustomerInfo)
router.get('/:customerID', GetCustomerContactInfo)
router.get('/machine/:customerID', GetCustomerMachineInfo)
router.get('/payment/:customerID', GetCustomerPaymentInfo)
router.post('/payDebt/cash',PayDebtCash)
router.post('/payDebt/installment',PayDebtInstallment)

function AddCustomer(req,res,next){
    var data=req.body
    var _result={}
    var orderID;
    var machineName = data.machineName
    var customerData=[data.customerFullname, data.customerCellphone, data.customerHomephone,date.getNowDate()]

    var customerSql="Insert into customer(customerFullname, customerCellphone, customerHomephone, creationDate) VALUES (?,?,?,?)"
    db.query(customerSql,customerData,function (err,result) {
        if(err){
            console.log(err)
            res.json({response : -2})
        }
        else {
            _result.customerID=result.insertId;
            var addressData=[data.city,data.district,data.neighborhood,data.street,data.apartmentNo,data.direction,data.longitude,data.latitude,1,_result.customerID]
            var addressSql="Insert into address(city, district, neighborhood, street, apartmentNo, direction, longitude, latitude, visible, customerID) VALUES (?,?,?,?,?,?,?,?,?,?)"
            db.query(addressSql,addressData,function (err,result) {
                if (err){
                    console.log(err)
                    res.json({response : -2})
                }
                else {
                    var machineOrderData = [machineName,date.getNowDate(),data.paymentType,data.assemblyDate,data.orderCost,_result.customerID]
                    var machineOrderSql="Insert into machine_order(machineName, creationDate, paymentType, assemblyDate, orderCost,customerID) VALUES (?,?,?,?,?,?)"
                    db.query(machineOrderSql,machineOrderData,function (err,result) {
                        if(err){
                            console.log(err)
                            res.json({response : -2})
                        }
                        else {
                            orderID=result.insertId;
                            var machineRepairData = [machineName,data.repairDate,0,_result.customerID]
                            var machineRepairSql="Insert into machine_repair(machineName, repairDate, done, customerID) VALUES (?,?,?,?)"
                            db.query(machineRepairSql,machineRepairData,function (err,result) {
                                if(err){
                                    console.log(err)
                                    res.json({response : -2})
                                }
                                else {
                                    if(data.paymentType==0){
                                        var cashPaymentData= [data.amountPaid,date.getNowDate(),machineName,_result.customerID,orderID]
                                        var cashPaymentSql = "Insert into cash_payment(amountPaid, paymentDate, machineName,customerID,orderID) VALUES (?,?,?,?,?)"
                                        db.query(cashPaymentSql,cashPaymentData,function (err,result) {
                                            if(err){
                                                console.log(err)
                                                res.json({response : -2})
                                            }
                                            else {
                                                res.json({response : 1,machineName:machineName})
                                            }
                                        })
                                    }
                                    else if(data.paymentType==1){
                                        var amountPaid=data.amountPaid
                                        var remainingDebt=(data.orderCost-amountPaid)/data.installmentCount
                                        var installmentPaymentInfoData = [data.startingDate,data.installmentCount,remainingDebt,_result.customerID,orderID]
                                        var installmentPaymentInfoSql="Insert into installment_payment_info(startingDate, installmentCount, installmentAmount,customerID,orderID) VALUES (?,?,?,?,?)"
                                        db.query(installmentPaymentInfoSql,installmentPaymentInfoData,function (err,result) {
                                            if (err){
                                                console.log(err)
                                                res.json({response :-2})
                                            }
                                            else{
                                                _result.installmentInfoID=result.insertId;
                                                var loopInstallmentPaymentSql="Insert into installment_payment(amountPaid, paymentDate, installmentInfoID) VALUES ?"
                                                var paymentDate = '';
                                                var paymentFormat =''
                                                var dataArray = new Array()
                                                const d = new Date();
                                                for(var i = 0;i<data.installmentCount;i++){

                                                    paymentDate = new Date(d.setMonth(d.getMonth()+1));
                                                    paymentFormat=new Date(paymentDate).getFullYear() + "-" + (new Date(paymentDate).getMonth() + 1) + "-" + new Date(paymentDate).getDate()
                                                    console.log(paymentFormat)
                                                    dataArray.push([0,paymentFormat,_result.installmentInfoID])
                                                    console.log(dataArray)
                                                    if(i==data.installmentCount-1)
                                                    {db.query(loopInstallmentPaymentSql,[dataArray],function (err,result) {
                                                        if(err){
                                                            console.log("Adding customer error ==>   "  + err)
                                                            res.json({response : -2})
                                                        }
                                                        else {
                                                            console.log("Adding customer Success")
                                                            res.json({response : 1})
                                                        }
                                                    })}

                                                }

                                            }
                                        })
                                    }
                                }
                            })
                        }

                    })
                }
            })
        }
    })
}

function GetCustomerInfo(req, res, next){
    var type=req.body.type
    if (type==1){
        var repairInfoSql="Select c.customerID,c.customerFullname, c.customerCellphone,DATEDIFF(mr.repairDate,NOW()) as date from customer c Inner JOIN machine_repair mr on c.customerID = mr.customerID"
        db.query(repairInfoSql,function (err,result) {
            if (err){
                console.log(err)
                res.json({response : -2})
            }
            else {
                res.json(result)
            }
        })
    }
    else if(type==2){
        var paymentInfoSql="Select c.customerID,c.customerFullname,c.customerCellphone,(mo.orderCost-cp.amountPaid) as debt from customer c Inner JOIN machine_order mo on c.customerID = mo.customerID Inner JOIN cash_payment cp on c.customerID = cp.customerID UNION SELECT c.customerCellphone,c.customerFullname,c.customerID,(m.orderCost-(SELECT SUM(amountPaid) from installment_payment ip where ip.installmentInfoID=ipi.installmentInfoID)) as debt from customer c Inner JOIN machine_order m on c.creationDate = m.creationDate Inner JOIN installment_payment_info ipi on c.customerID = ipi.customerID ORDER BY debt DESC"
        db.query(paymentInfoSql,function (err,result) {
            if(err){
                console.log(err)
                res.json({response : -2})
            }
            else {
                res.json(result)
            }
        })
    }
    else if(type==3){
        var mapInfoSql="Select a.latitude,a.longitude,c.customerID,c.customerFullname,c.customerCellphone,DATEDIFF(mr.repairDate,NOW()) as date from customer c Inner JOIN address a on c.customerID = a.customerID Inner JOIN machine_repair mr on c.customerID = mr.customerID"
        db.query(mapInfoSql,function (err,result) {
            if(err){
                console.log(err)
                res.json({response : -2})
            }
            else {
                res.json(result)
            }
        })
    }
    else console.log("type undefined")
}

function GetCustomerContactInfo(req,res,next){
    var sql = "Select c.customerFullname,c.customerCellphone,c.customerHomephone,a.city,a.district,a.neighborhood,a.street,a.apartmentNo,a.direction,a.longitude,a.latitude from customer c Inner JOIN address a on c.customerID = a.customerID where a.visible=1 and c.customerID=?"
    db.query(sql,req.params.customerID,function (err,result) {
        if (err){
            console.log(err)
            res.json({response :-2})
        }
        else {
            res.json(result)
        }
    })
}

function GetCustomerMachineInfo(req,res,next){
    var sql="Select DATEDIFF(mr.repairDate,NOW()) as repairDate,mo.machineName, DATE_FORMAT(mo.assemblyDate,'%d/%m/%Y') AS assemblyDate from customer c Inner JOIN machine_repair mr on c.customerID = mr.customerID INNER JOIN machine_order mo on mr.machineName = mo.machineName where mo.customerID=?"
    db.query(sql,req.params.customerID,function (err,result) {
        if (err){
            console.log(err)
            res.json({response : -2})
        }
        else {
            res.json(result)
        }
    })
}

function GetCustomerPaymentInfo(req,res,next){
    var customerID= req.params.customerID
    var cash_sql="Select '0' as type, mo.orderCost,cp.amountPaid from cash_payment cp Inner JOIN machine_order mo on cp.orderID = mo.orderID where mo.customerID=?"
    db.query(cash_sql,customerID,function (err,result) {
        if (err){
            console.error(err)
            res.json({response : -2})
        }
        else if(result==''){
            var installmenyt_sql="Select '1' as type,ip.paymentID, mo.orderCost,ipi.installmentCount, DATE_FORMAT(ip.paymentDate,'%d/%m/%Y') AS paymentDate, ip.paymentStatus, ipi.installmentAmount from installment_payment_info ipi Inner JOIN machine_order mo on ipi.orderID = mo.orderID Inner JOIN installment_payment ip on ip.installmentInfoID=ipi.installmentInfoID where mo.customerID=?"
            db.query(installmenyt_sql,customerID,function (err,result) {
                if (err){
                    console.error(err)
                    res.json({response : -2})
                }
                else if(result==''){
                    res.json({response : 0})
                }
                else {
                    res.json(result)
                }
            })
        }
        else {
            res.json(result)
        }
    })
}

function PayDebtCash(req,res,next){
    var sql="Update cash_payment set amountPaid=amountPaid+? where customerID=?"
    db.query(sql,[req.body.amountPaid,req.body.customerID],function (err,result) {
        if (err){
            console.error(err)
            res.json({response : -2})
        }
        else {
            res.json({response : 1})
        }
    })
}

function PayDebtInstallment(req,res,next){
    var sql="Update installment_payment set paymentStatus=1 where paymentID=?"
    db.query(sql,req.body.paymentID,function (err,result) {
        if (err){
            console.error(err)
            res.json({response : -2})
        }
        else {
            res.json({response : 1})
        }
    })
}

module.exports=router;
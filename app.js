var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mysql = require('mysql');

var loginRouter = require('./routes/login');
var reqisterRouter = require('./routes/register');
var stock = require('./routes/stock')
var product = require('./routes/product')
var user = require('./routes/user')
var customer = require('./routes/customer')

var app = express();

var port=3010
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : 'neslihan12345',
  database : 'yazilimbakimi',


});

connection.connect(function(err){
  if(!err) {
    console.log("Database is connected ... nn");
  } else {
    console.log("Error connecting database ... nn");
  }
});
global.db = connection;

setInterval(function () {
  db.query('SELECT 1');
}, 5000);
app.use(express.static(path.join(__dirname, 'public')));
app.use('/images', express.static('/images'))

app.use('/login', loginRouter);
app.use('/register', reqisterRouter);
app.use('/stock', stock);
app.use('/product', product);
app.use('/user', user);
app.use('/customer', customer);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
app.listen(port, () => {
  console.log(`Server running on port: ${port}`);
});
module.exports = app;

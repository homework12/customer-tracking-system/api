exports.getNowDate= () => {
    const d = new Date();
    return d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
};

exports.getNowForPath = () =>{
    const now = new Date();
    return now.getUTCFullYear() +
        ("0" + (now.getUTCMonth() + 1)).slice(-2) +
        ("0" + now.getUTCDate()).slice(-2) +
        ("0" + now.getUTCHours()).slice(-2) +
        ("0" + now.getUTCMinutes()).slice(-2) +
        ("0" + now.getUTCSeconds()).slice(-2);
}